package com.nespresso.exercise.electric_trip;

public class ElectricCar {
	private int batterySize; // KWh
	private int lowSpeedPerformance; // Km per KWh
	private int highSpeedPerformance; // Km per KWh
	private String location;
	private double chargeOfBattery;

	public ElectricCar(int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance, String location) {
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
		this.location = location;
		this.chargeOfBattery = batterySize;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + batterySize;
		result = prime * result + highSpeedPerformance;
		result = prime * result + lowSpeedPerformance;
		return result;
	}

	public int getBatterySize() {
		return batterySize;
	}

	public void setBatterySize(int batterySize) {
		this.batterySize = batterySize;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void go(Trip trip) {
		this.location = trip.getTo();
		this.chargeOfBattery = (this.chargeOfBattery - ((double) trip
				.getDistance()) / this.lowSpeedPerformance);
	}

	public void sprint(Trip trip) {
		this.location = trip.getTo();
		this.chargeOfBattery = (this.chargeOfBattery - ((double) trip
				.getDistance()) / this.highSpeedPerformance);
	}

	public Double getChargeOfBattery() {
		return this.chargeOfBattery;
	}

	public String getChargeOfBatteryPercent() {
		return Math.round(100 * this.chargeOfBattery / this.batterySize) + "%";
	}

	public boolean isEnoughCharge(Trip t,String speed) {
		int s="sprint".equals(speed)?this.highSpeedPerformance:this.lowSpeedPerformance;
		double estimation = (this.chargeOfBattery - ((double) t.getDistance()) / s);
		return estimation > 0;
	}

	public void charge(int hours, int charge) {
		double newCharge = this.chargeOfBattery + hours * charge;
		if (newCharge < this.batterySize)
			this.chargeOfBattery = newCharge;
		else
			this.chargeOfBattery = this.batterySize;
	}
}
