package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ElectricTrip {

	private Map<String, Trip> trips = new HashMap<String, Trip>();
	private int totalDistance;
	private Map<Integer, ElectricCar> cars = new HashMap<Integer, ElectricCar>();

	public ElectricTrip(String s) {
		// "PARIS-200-BOURGES"
		if (s != null) {
			String[] arrayTrip = s.split("-");
			for (int i = 0; i + 1 < arrayTrip.length; i = i + 2) {
				int distance=Integer.parseInt(arrayTrip[i + 1]);
				totalDistance+=distance;
				Trip trip = new Trip(arrayTrip[i], arrayTrip[i + 2],
						distance);
				trips.put(trip.getFrom(), trip);
			}
		}
	}

	public int startTripIn(String from, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
		ElectricCar car = new ElectricCar(batterySize, lowSpeedPerformance,
				highSpeedPerformance, from);
		int id = car.hashCode();
		cars.put(id, car);
		return id;
	}

	public void charge(int id, int hoursOfCharge) {
		if (cars.containsKey(id)) {
			ElectricCar car = cars.get(id);
			for (Entry<String, Trip> entry : trips.entrySet()) {
				if (trips.containsKey(car.getLocation())) {
					Trip trip = trips.get(car.getLocation());
					car.charge(hoursOfCharge, trip.getChargedPerHour());
					return;
				}
			}

		}
	}

	public void go(int id) {
		this.go(id,"go");
	}
	
	private void go(int id,String speed){
		if (cars.containsKey(id)) {
			ElectricCar car = cars.get(id);
			for (Entry<String, Trip> entry : trips.entrySet()) {
				if (trips.containsKey(car.getLocation())) {
					Trip trip = trips.get(car.getLocation());
					if (car.isEnoughCharge(trip,speed)) {
						if("go".equals(speed)){
							car.go(trip);
						}else if ("sprint".equals(speed)){
							car.sprint(trip);
						}
						if(trip.isChargeInDestination()){
							return;
						}
					}

				}
			}

		}
	}

	public void sprint(int id) {
		this.go(id,"sprint");
	}

	public String locationOf(int id) {
		if (cars.containsKey(id))
			return cars.get(id).getLocation();
		else
			return "";
	}

	public String chargeOf(int id) {
		if (cars.containsKey(id))
			return cars.get(id).getChargeOfBatteryPercent();
		else
			return "";
	}
}
