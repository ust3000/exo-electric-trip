package com.nespresso.exercise.electric_trip;

public class Trip {
	private String from;
	private int chargedPerHour;
	private String to;
	private int distance;
	private boolean isChargeInDestination;

	public Trip(String from, String to, int distance) {
		String[] s=from.split(":");
		if(s.length>1){
			this.from=s[0];
			this.chargedPerHour=Integer.parseInt(s[1]);
		}else{
			this.from=from;
		}
		String[] e=to.split(":");
		if(e.length>1){
			this.to=e[0];	
			isChargeInDestination=true;
		}else {
			this.to=to;	
		}

		this.distance=distance;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public int getDistance() {
		return distance;
	}

	public int getChargedPerHour() {
		return chargedPerHour;
	}

	public boolean isChargeInDestination() {
		return isChargeInDestination;
	}
	


}
